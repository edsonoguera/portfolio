import React from 'react'
import { Switch, Route, Link } from 'react-router-dom';
import HomePage from '../pages/homepage'

const AppRoutes = (props) => {
    return (
        <Switch>
            <Route exact path='/'>
                <HomePage />
            </Route>
            <Route exact path='/projetos'>
                <h1>Projetos</h1>
            </Route>
            <Route exact path='/sobre'>
                <h1>Sobre mim</h1>
            </Route>
        </Switch>
    )
}

export default AppRoutes;