import React from 'react';
import './App.css';
import AppRoutes from '../src/routes/'

function App() {
  return (
    <div className="App">
      <AppRoutes />
    </div>
  );
}

export default App;
