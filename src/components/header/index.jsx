import React from 'react'

const Header = (props) => {
    return (
        <section>
            <header>
                {props.children}
            </header>
        </section>
    )
}

export default Header;