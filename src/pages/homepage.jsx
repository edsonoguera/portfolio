import React from 'react';
import Header from '../components/header/';
import { Link } from 'react-router-dom';

const HomePage = () => {
    return (
        <div>
            <Header>
                <Link to='/'>Home</Link>
                <Link to='/projetos'>Projetos</Link>
                <Link to='sobre'>Sobre</Link>
            </Header>
            <h1>Homepage</h1>
        </div>
    )
}

export default HomePage;